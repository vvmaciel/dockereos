#!/bin/bash

alias cleos='docker exec -it nodeos /opt/eosio/bin/cleos --url http://127.0.0.1:8888 --wallet-url http://i127.0.0.1:9876'
alias eosio-cpp='docker run -it --rm -w /home -v $(pwd):/home  eosio/cdt eosio-cpp' 
alias cdtbash='docker run -it --rm -w /home -v $(pwd):/home  eosio/cdt bash' 
alias eosiocpp='docker run -it --rm -w /home -v $(pwd):/home eosio/eos-dev eosiocpp' 
